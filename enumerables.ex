defmodule Enumerables do
  def main do
    # Enum is a data type that allow its item to counted off
    # check "All" the list contain even elements
    IO.puts "Even List : #{Enum.all?([1,2,3],
                           fn(n) -> rem(n, 2) == 0 end)}"

    # check "Any" of the list element contain even
    IO.puts "Any of them even in the list: #{Enum.any?([1,2,3],
                                              fn(n) -> rem(n, 2) == 0 end)}"

    # print each list element
    Enum.each([1,2,3], fn(n) -> IO.puts n end)

    # double list element
    db_list = Enum.map([1,2,3], fn(n) -> n *2 end)
    IO.inspect db_list

    # reduce down to a single value
    sum_vals = Enum.reduce([1,2,3], fn(n, sum) -> n + sum end)
    IO.puts "Sum: #{sum_vals}"

    IO.inspect Enum.uniq([1,2,2])
  end

end
