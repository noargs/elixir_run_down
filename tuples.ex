defmodule Tuples do
  def main do
    # Normally tuples have 2 to 4 values in them
    # they can hold any value
    # However they are not for enumerating through
    #   - i.e. you are not supposed to cycle through tuples
    my_stats = {175, 6.2, :Pk}

    IO.puts "Tuple #{is_tuple(my_stats)}"

    # you can append/extends tuple but you vae to assign it to new variables
    my_stats2 = Tuple.append(my_stats, 42)

    # Retrieve value by index
    IO.puts "Age #{elem(my_stats2, 3)}"

    IO.puts "Size: #{tuple_size(my_stats2)}"

    # delete a tuple element by index
    my_stat3 = Tuple.delete_at(my_stats2, 0)

    # insert in tuple at specific index
    my_stats4 = Tuple.insert_at(my_stat3, 0, 1974)

    # create tuple with certain value duplicated in certain number of times
    many_zeroes = Tuple.duplicate(0,5)

    # pattern matching with tuples
    {weight, height, name} = {175, 6.25, "Pk"}
    IO.puts "Weight: #{weight}"

  end

end
















