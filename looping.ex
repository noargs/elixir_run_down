defmodule Looping do
  def main do

    # add list elements
    IO.puts "Sum: #{sum([1,2,3])}"

    # start loop at 5 until 1 NOT 0
    loop(5,1)
  end

  def sum([]), do: 0
  def sum([h|t]), do: h + sum(t)

  def loop(0, _), do: nil
  def loop(max, min) do
    if max < min do
      loop(0, min)
    else
      IO.puts "Num: #{max}"
      loop(max - 1, min)
    end
  end

end
