defmodule Concur do
  def main do
    spawn(fn() -> loop(50, 1) end)
    spawn(fn() -> loop(100, 1) end)

    send(self(), {:french, "Bob"})

    receive do
      {:german, name} -> IO.puts "Guten tag #{name}"
      {:french, name} -> IO.puts "Bonjure #{name}"
      {:english, name} -> IO.puts "Hello #{name}"
      after
        100 -> IO.puts "Time up"
    end
  end

  def loop(0, _), do: nil

  def loop(max, min) do
    if max < min do
      loop(0, min)
    else
      IO.puts "N: #{max}"
      loop(max - 1, min)
    end
  end

end
