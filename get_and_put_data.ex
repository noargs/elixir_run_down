# "cd" into elixir_w/learn_elixir/start
# type in terminal $ iex
# c("get_and_put_data.ex")
# to "run" M.main
defmodule M do
  def main do
    name = IO.gets("What is your name? ") |> String.trim
    IO.puts "Hello, #{name}"
  end
end