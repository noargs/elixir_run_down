defmodule Logical do
  def main do
    age = 16
    IO.puts "Vote & Drive: #{(age >= 16) and (age >= 18)}"
    IO.puts "Vote or Drive: #{(age >= 16) or (age >= 18)}"

    IO.puts not true
  end
end
