defmodule Lists do
  def main do
    list1 = [1,2,3]
    list2 = [4,5,6]

    list3 = list1 ++ list2

    list4 = list3 -- list1

    # verify if an item is inside the list
    IO.puts 6 in list4

    # create list of key value tuples
    my_stats = [name: "Superman", height: 6.40]

    [head|tail] = list3
    IO.puts "Head: #{head}"

    IO.write "Tail: #{tail}" # "write" doesnt put new line as "puts" does
    IO.inspect tail  # inspect print the internal represantation

    IO.inspect [97,98] # it will print [97,98]
    IO.inspect [97,98], char_lists: :as_lists # make sure it only print char

    # Enumerate over list (cycle through list)
    Enum.each tail, fn item ->
      IO.puts item
    end

    words = ["Random", "Words", "in a", "list"]
    Enum.each words, fn word ->
      IO.puts word
    end

    # use recursion to loop
    display_list(words)

    # delete given item in the list
    IO.puts display_list(List.delete(words, "Random"))

    # delete an item at specific index
    IO.puts display_list(List.delete_at(words, 1))

    # insert item in the list
    IO.puts display_list(List.insert_at(words, 4, "Yeah"))

    # get first item inside list
    IO.puts List.first(words)

    # get last item inside list
    IO.puts List.last(words)

  end

  def display_list([word|words]) do
    IO.puts word
    display_list(words)
  end
  def display_list([]), do: nil

end
