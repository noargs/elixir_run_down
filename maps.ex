defmodule Maps do
  def main do
    # map holds collection of key/value pair

    # key as string
    capitals = %{"Alabama" => "Montgomery",
      "Alaska" => "Juneau", "Arizona" => "Phoenix"}

    IO.puts "Capital of Alaska is #{capitals["Alaska"]}"

    # key as tuples
    capitals2 = %{alabama: "Montgomery",
    alaska: "Juneau", arizona: "Phoenix"}

    IO.puts "Capital of Arizona is #{capitals2.arizona}"

    capitals3 = Map.put_new(capitals, "Arkansas", "Little Rock")

    IO.puts capitals3

  end

end
