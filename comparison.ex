defmodule Comparison do
  def main do
    IO.puts "4 == 4.0: #{4 == 4.0}"
    IO.puts "4 === 4.0: #{4 === 4.0}"
    IO.puts "4 != 4.0: #{4 != 4.0}"
    IO.puts "4 !== 4.0: #{4 !== 4.0}"

    IO.puts "5 > 4: #{5 > 4}"
    IO.puts "5 >= 4: #{5 >= 4}"
    IO.puts "5 < 4: #{5 < 4}"
    IO.puts "5 <= 4: #{5 <= 4}"
  end
end
