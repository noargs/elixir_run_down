defmodule Data do
  def main do
    # Int
    my_int = 123
    IO.puts "Integer #{is_integer(my_int)}"

    # Float
    my_float = 3.12159
    IO.puts "Float #{is_float(my_float)}"

    # Atom
    IO.puts "Atom #{is_atom(:Pittsburgh)}" # atom with space :"New York"

    # Ranges
    one_to_10 = 1..10

    # Strings
    my_str = "My Sentence"
    # -String functions
    # length
    IO.puts "Length: #{String.length(my_str)}"
    # Concat
    longer_str = my_str <> " " <> "is longer"
    # value as well as data type is equal "==="
    IO.puts "Equal: #{"Egg" === "egg"}"
    # String contains another string
    IO.puts "My ? #{String.contains?(my_str, "My")}"
    # get first letter in a string
    IO.puts "First: #{String.first(my_str)}"
    # get character at specified index
    IO.puts "Index 4: #{String.at(my_str, 4)}"
    # get substring (start from 3 until 8th)
    IO.puts "Substring: #{String.slice(my_str, 3, 8)}"
    # split string into list; "inspect" prints the internal representation of a string
    IO.inspect String.split(longer_str, " ")
    IO.puts "Reverse a string: #{String.reverse(longer_str)}"
    IO.puts "Uppercase: #{String.upcase(longer_str)}"
    IO.puts "Downcase: #{String.downcase(longer_str)}"
    IO.puts "Capitalize: #{String.capitalize(longer_str)}"
    # pipe a value/result to something
    4 * 10 |> IO.puts
  end
end










