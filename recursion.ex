defmodule Recursion do
  def main do
    IO.puts "Factorial of 4: #{factorial(4)}"
  end


  # 1st : result = 4 * factorial(3) = 4 * 6 = 24
  # 2nd : result = 3 * factorial(2) = 3 * 2 = 6
  # 3rd : result = 2 * factorial(1) = 2 * 1 = 2
  def factorial(n) do
    if n <= 1 do
      1
      else
      result = n * factorial(n - 1)
      result # return a value just put it by itself
    end
  end

end
