defmodule ListComprehension do
  def main do
    db_list = for n <- [1,2,3], do: n * 2
    IO.inspect db_list

    even_list = for n <- [1,2,3,4], rem(n, 2) == 0, do: n
    IO.inspect even_list
  end

end
