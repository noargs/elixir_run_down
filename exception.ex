defmodule Exception do
  def main do

    err = try do
      5 / 0
    rescue
      ArithmeticError -> "Can't Divide by Zero"
    end

    IO.puts err

  end

end
